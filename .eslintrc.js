// module.exports = {
//   root: true,
//   extends: '@react-native-community',
// };
module.exports = {
  'extends': '@react-native-community',
  'parser': 'babel-eslint',
  'env': {
    'jest': true,
  },
  'rules': {
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    'comma-dangle': 'off'
  },
  'globals': {
    "fetch": false
  }
}
// module.exports = {
//   "plugins": [
//     "react",
//     "react-native",
//     "prettier"
//   ],
//   "parserOptions": {
//     "ecmaVersion": 2018,
//     "ecmaFeatures": {
//       "jsx": true,
//       "modules": true
//     },
//     "sourceType": "module",
//     "useJSXTextNode": false
//   },
//   "env": {
//     "react-native/react-native": true
//   },
//   "extends": [
//     "standard",
//     "plugin:react/recommended",
//     "plugin:react-native/all",
//     "plugin:prettier/recommended"
//   ],
//   "settings": {
//     "react": {
//       "version": "detect",
//     },
//   },
//   "rules": {
//     "react-native/no-raw-text": 0 // Avoid false positive, wait for fix
//   }
// }