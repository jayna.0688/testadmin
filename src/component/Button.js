import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Global from '@utils/GlobalValue';
import EStyleSheet from 'react-native-extended-stylesheet';

const Button_Height = 60;
const Button_Width = Math.round(Button_Height * 3.16);
const BorderRadius = Math.round(Button_Height / 2);
const BorderWidth = 4;
const BorderColor = Global.C_YELLOW;

const RoundButton = ({onPress, label}) => (
  <TouchableOpacity onPress={onPress} style={styles.button_wrapper}>
    <Text style={styles.button_text}>{label}</Text>
  </TouchableOpacity>
);

export {RoundButton};


const styles = EStyleSheet.create({
  button_wrapper: {
    width: Button_Width,
    height: Button_Height,
    borderRadius: BorderRadius,
    borderWidth: BorderWidth,
    borderColor: BorderColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    fontFamily: Global.F_Avenir_Bold,
    fontWeight: 'bold',
    fontSize: '20rem',
    color: Global.C_YELLOW,
    lineHeight: Button_Height,
    letterSpacing: 2,
  },
});
