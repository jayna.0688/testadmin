import React from 'react';
import { Text } from 'react-native';
import Global from '@utils/GlobalValue';
import EStyleSheet from 'react-native-extended-stylesheet';

let getStyle = function (family, size, color, textstyle, type) {
  let fontFamily = '',
    fontWeight = '';
  if (family === 'bold') {
    fontFamily = Global.F_Avenir_Bold;
    fontWeight = 'bold';
  } else if (family === 'demi') {
    fontFamily = Global.F_Avenir_Demi;
    fontWeight = 'normal';
  } else {
    fontFamily = Global.F_Avenir_Reg;
    fontWeight = 'normal';
  }
  const fontSize = size + 'rem';
  return EStyleSheet.create({
    text: {
      fontFamily: fontFamily,
      fontWeight: fontWeight,
      fontSize: fontSize,
      color: color,
      letterSpacing: type,
      ...textstyle,
    },
  });
};

const TextS2 = ({ family, size, color, label, textstyle }) => {
  let style = getStyle(family, size, color, textstyle, 2);

  return <Text style={style.text}>{label}</Text>;
};
const TextS1 = ({ family, size, color, label, textstyle }) => {
  let style = getStyle(family, size, color, textstyle, 1);

  return <Text style={style.text}>{label}</Text>;
};
const TextS0 = ({ family, size, color, label, textstyle }) => {
  let style = getStyle(family, size, color, textstyle, 0);

  return <Text style={style.text}>{label}</Text>;
};

export { TextS0, TextS1, TextS2 };