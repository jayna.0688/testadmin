import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const MenuIcon = ({ color, onPress }) => (
    <TouchableOpacity onPress={onPress} style={styles.menuicon}>
        <View style={[styles.menuicon_top, { backgroundColor: color }]} />
        <View style={[styles.menuicon_bottom, { backgroundColor: color }]} />
    </TouchableOpacity>
);

export default MenuIcon;

const styles = EStyleSheet.create({
    menuicon: {
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    menuicon_top: {
        width: '18rem',
        height: '4rem',
    },
    menuicon_bottom: {
        marginTop: '4rem',
        width: '13rem',
        height: '4rem',
    },
});
