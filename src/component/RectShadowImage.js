
import React from 'react';
import { Image, View, TouchableOpacity, StyleSheet } from 'react-native';
import Global from '@utils/GlobalValue';

const RectShadowImage = ({ onPress, width, image }) => (

    <TouchableOpacity
        onPress={onPress}
        style={[styles.container, { width: width }]}>

        <View style={styles.image_wrapper}>
            <Image
                source={image}
                style={styles.image}
                resizeMode='contain' />
        </View>

    </TouchableOpacity>
);

export default RectShadowImage;

const styles = StyleSheet.create({
    container: {
        aspectRatio: 1.2,
        backgroundColor: Global.C_GRAYBACK,
        borderRadius: 5,
        elevation: 10,
        shadowColor: 'gray',
        shadowOpacity: 0.5,
        shadowOffset: { width: 0, height: 5 },
        shadowRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image_wrapper: {
        width: '40%',
        height: '50%',
    },
    image: {
        width: '100%',
        height: '100%',
    },
});
