
import React from 'react';
import { View, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
import Global from '@utils/GlobalValue';
import ArrowTextHeader from '@component/ArrowTextHeader';
const bloodchecklist = {
    text: 'Tick the items off the list below if you have been tested for the indicated values (do this with your doctor)',
    info: [
        {
            category: 'LABOR',
            items: ['Blutbild mit Differentialblutbild', 'Gesamt lgG im Serum', 'Leukozyten - Subpopulationen', 'C-reaktives Protein (CRP)', 'Urinstatus', 'Schwangersschaftstest (bei Frauen im gebährfähigen Alter)']
        },
        {
            category: 'SEROLOGIE',
            items: ['Hepatitis B', 'Hepatitis C', 'HIV', 'Varizella zoster Virus', 'Tuberkulose', 'JC-Virus (bei Vortherapie mit Natalizumab)']
        }
    ]
}
const vaccinationchecklist = {
    text: 'Tick the items off the list below if you have been vaccinated (do this with your doctor)',
    info: [
        {
            category: 'IMPFSTATUS',
            items: ['Grundimmunisierung nach STIKO Impfkalender\n(Tetanus, Diphterie, Pertussis, Poliomyelitis, Haemophilus influenzae Typ B, Rotaviren, Meningokokken, Masem, Mumps, Röteln, Verizellen)', 'Saisonale Influenza (jährlich)', 'Pneumokokken (falls noch nicht vorhanden)', 'Auffrischimpfung Tetanus / Diphterie (alle 10 Jahre)', 'Hepatitis B']
        }
    ]
}


export default class Checklist extends React.Component {
    render() {
        return (
            <View style={{ flex: 1 }} >
                {/* header */}
                <ArrowTextHeader title={'Blood test checklist'} onPress={() => this.props.navigation.goBack()} />

                <ScrollView>
                    <View style={{width:'70%', alignSelf:'center', marginVertical:30}}>
                        <TextS0 family="reg" size="14" color={Global.C_BACKGROUND} label={bloodchecklist.text} textstyle={{textAlign:'center',lineHeight:16}} />
                    </View>
                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({

});
