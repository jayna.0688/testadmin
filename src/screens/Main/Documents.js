
import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
import Global from '@utils/GlobalValue';
import ArrowTextHeader from '@component/ArrowTextHeader';
import RectShadowImage from '@component/RectShadowImage';
const itemRectWidth = Math.round(Global.ScreenWidth * 0.4);
const itemImages = [
    require('@images/inject.png'),
    require('@images/blood.png'),
];

export default class Documents extends React.Component {
    render() {
        return (
            <View style={{ flex: 1 }} >

                {/* header */}
                <ArrowTextHeader title={'Documents'} onPress={() => this.props.navigation.goBack()} />

                <View style={{ height: Math.round(Global.ScreenHeight * 0.15), justifyContent: 'center', alignItems: 'center' }}>
                    <TextS0 family="reg" size="12" color={Global.C_BACKGROUND} label={'Below you can find all the necessary'} />
                    <TextS0 family="reg" size="12" color={Global.C_BACKGROUND} label={'tests that you need to have'} />
                    <TextS0 family="reg" size="12" color={Global.C_BACKGROUND} label={'completed before your therapy day.'} />
                </View>

                <View style={{ marginVertical: 15, alignSelf:'center' }}>
                    <RectShadowImage width={itemRectWidth} image={itemImages[0]} />
                </View>

                <View style={{ marginVertical: 15, alignSelf:'center' }}>
                    <RectShadowImage width={itemRectWidth} image={itemImages[1]} />
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({

});
