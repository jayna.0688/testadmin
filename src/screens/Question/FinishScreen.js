import React from 'react';
import {View, StyleSheet} from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
import Global from '@utils/GlobalValue';
import {saveData} from '@utils/GlobalFunction';
import RNRestart from 'react-native-restart';
import {RoundButton} from '@component/Button';
// const StartBtn_Height = 60;
// const StartBtn_Width = Math.round(StartBtn_Height * 3.16);

export default class FinishScreen extends React.Component {
  pressEnter() {
   // saveData('QEnd', '1');
   // RNRestart.Restart();
  }

  render() {
    return (
      <View style={styles.container}>
        <TextS2 family="bold" size="28" color="white" label="A warm welcome" />

        <View style={styles.description_wrapper}>
          <TextS2
            family="reg"
            size="18"
            color="white"
            label="Welcome to Ocrevus Butler,"
          />
          <TextS2
            family="reg"
            size="18"
            color="white"
            label={'your Ocrevus\u24C7 treatment'}
          />
          <TextS2 family="reg" size="18" color="white" label="companion." />
          <TextS2
            family="reg"
            size="18"
            color="white"
            label="Everything you need to get"
          />
          <TextS2
            family="reg"
            size="18"
            color="white"
            label="ready for your therapy day,"
          />
          <TextS2
            family="reg"
            size="18"
            color="white"
            label="you will find here."
          />
          <TextS2 family="reg" size="18" color="white" label="" />
          <TextS2
            family="reg"
            size="18"
            color="white"
            label="Enter the app and discover"
          />
          <TextS2
            family="reg"
            size="18"
            color="white"
            label={"it's most important features"}
          />
        </View>

        <View style={styles.enterbtn_wrapper}>
          <RoundButton label="ENTER" onPress={this.pressEnter} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  description_wrapper: {
    marginTop: 30,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Global.C_BACKGROUND,
  },
  enterbtn_wrapper: {
    position: 'absolute',
    bottom: Math.round(Global.ScreenHeight * 0.1),
  },
});
