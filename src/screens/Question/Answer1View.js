import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
import Global from '@utils/GlobalValue';
import Moment from 'moment';
import isEmpty from '@utils/isEmpty';
import {getData} from '@utils/GlobalFunction';
import Picker from 'react-native-wheel-picker';
var PickerItem = Picker.Item;

const Btn_Width = Math.round(Global.ScreenWidth * 0.33);
const Btn_Height = Math.round(Global.ScreenWidth * 0.14);
const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sept',
  'Oct',
  'Nov',
  'Dec',
];
const C_BTN_DESELECT = '#8C95BF';
const C_BTN_SELECT = 'white';

export default class Answer1Screen extends React.Component {
  constructor() {
    super();
    this.state = {
      selected_year: 0,
      selected_month: 0, // 0~11
      selected_date: 1,
      years_list: [''],
      dates_list: [''],
      selected_no: false,
      selected_yes: false,
      init_data: {
        year: 0,
        month: 0,
        date: 0,
      },
    };
    // this.setDateList = this.setDateList.bind(this);
  }

  UNSAFE_componentWillMount = async () => {
    const today = new Date();
    const today_year = today.getFullYear();
    const today_month = today.getMonth();
    const today_date = today.getDate();

    let years_list = [];
    for (let i = 0; i < 10; i++) {
      years_list.push((today_year + i).toString());
    }

    const date_str = await getData('a1');
    if (isEmpty(date_str)) {
      this.setDateList(today_year, today_month);
      this.setState({
        years_list: years_list,
        selected_month: today_month,
        selected_date: today_date - 1,
        init_data: {
          year: 0,
          month: today_month,
          date: today_date - 1,
        },
      });
    } else {
      const year = date_str.substring(0, 4);
      const month = parseInt(date_str.substring(4, 6), 10) - 1;
      const date = parseInt(date_str.substring(6, 8), 10) - 1;
      this.setDateList(year, month);
      let i = 0;
      while (years_list[i] !== year && i < 10) {
        i++;
      }
      const selected_year = i;
      this.setState({
        years_list: years_list,
        selected_year: selected_year,
        selected_month: month,
        selected_date: date,
        selected_no: false,
        selected_yes: true,
        init_data: {
          year: selected_year,
          month: month,
          date: date,
        },
      });
    }
  };

  setDateList(year, month) {
    let monthString = (month + 1).toString();
    if (month < 9) {
      monthString = '0' + monthString;
    }
    const momentString = year + '-' + monthString;
    const nDaysOfMonth = Moment(momentString, 'YYYY-MM').daysInMonth();
    let dates_list = [];
    for (let i = 0; i < nDaysOfMonth; i++) {
      let date_str = (i + 1).toString();
      if (i < 9) {
        date_str = '0' + date_str;
      }
      dates_list.push(date_str);
    }
    this.setState({
      dates_list: dates_list,
    });
    if (this.state.selected_date >= nDaysOfMonth) {
      this.setState({selected_date: nDaysOfMonth - 1});
    }
  }

  onPickerSelect(picker, index) {
    let selected_date = this.state.selected_date;
    let selected_month = this.state.selected_month;
    let selected_year = this.state.selected_year;
    if (picker === 1) {
      // date
      this.setState({
        selected_date: index,
      });
      selected_date = index;
    } else if (picker === 2) {
      // month
      this.setState({
        selected_month: index,
      });
      selected_month = index;
      this.setDateList(this.state.years_list[this.state.selected_year], index);
    } else if (picker === 3) {
      // year
      this.setState({
        selected_year: index,
      });
      selected_year = index;
    }

    if (
      this.state.init_data.year !== selected_year ||
      this.state.init_data.month !== selected_month ||
      this.state.init_data.date !== selected_date
    ) {
      let month_2num = (selected_month + 1).toString();
      if (selected_month < 9) {
        month_2num = '0' + month_2num;
      }
      const data_str =
        this.state.years_list[selected_year].toString() +
        month_2num +
        this.state.dates_list[selected_date];
      const data = {
        screen: 1,
        save: data_str,
      };
      this.props.onChanged(data);
    }
  }

  pressYesNo(isYes) {
    this.setState({
      selected_yes: isYes,
      selected_no: !isYes,
    });
  }

  render() {
    const underlineTextStyleObj = {
      textDecorationLine: 'underline',
      textDecorationColor: 'white',
    };
    return (
      <View style={styles.container}>
        <View style={styles.yesno_wrapper}>
          <TouchableOpacity
            onPress={this.pressYesNo.bind(this, false)}
            style={[
              styles.btn_wrapper,
              {
                backgroundColor: this.state.selected_no
                  ? C_BTN_SELECT
                  : C_BTN_DESELECT,
              },
            ]}>
            <TextS2
              family="bold"
              size="20"
              color={this.state.selected_no ? Global.C_BACKGROUND : 'white'}
              label="NEIN"
              textstyle={{lineHeight: Btn_Height}}
            />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={this.pressYesNo.bind(this, true)}
            style={[
              styles.btn_wrapper,
              {
                backgroundColor: this.state.selected_yes
                  ? C_BTN_SELECT
                  : C_BTN_DESELECT,
              },
            ]}>
            <TextS2
              family="bold"
              size="20"
              color={this.state.selected_yes ? Global.C_BACKGROUND : 'white'}
              label="JA"
              textstyle={{lineHeight: Btn_Height}}
            />
          </TouchableOpacity>
        </View>

        {this.state.selected_yes && (
          <View style={styles.picker_container}>
            <Picker
              style={styles.picker}
              selectedValue={this.state.selected_date}
              itemStyle={styles.picker_item}
              onValueChange={index => this.onPickerSelect(1, index)}>
              {this.state.dates_list.map((value, i) => (
                <PickerItem label={value} value={i} key={'date' + value} />
              ))}
            </Picker>

            <Picker
              style={styles.picker}
              selectedValue={this.state.selected_month}
              itemStyle={styles.picker_item}
              onValueChange={index => this.onPickerSelect(2, index)}>
              {months.map((value, i) => (
                <PickerItem label={value} value={i} key={'month' + value} />
              ))}
            </Picker>

            <Picker
              style={styles.picker}
              selectedValue={this.state.selected_year}
              itemStyle={styles.picker_item}
              onValueChange={index => this.onPickerSelect(3, index)}>
              {this.state.years_list.map((value, i) => (
                <PickerItem label={value} value={i} key={'year' + value} />
              ))}
            </Picker>
          </View>
        )}
        {this.state.selected_no && (
          <View style={styles.subcontainer_no}>
            <TextS2
              family="reg"
              size="14"
              color={'white'}
              label="You can only use Ocrevus Butler"
            />
            <TextS2
              family="reg"
              size="14"
              color={'white'}
              label="with an upcoming treatment."
            />

            <TouchableOpacity style={styles.spacer1}>
              <TextS2
                family="reg"
                size="14"
                color={'white'}
                label="help / contact link here"
                textstyle={underlineTextStyleObj}
              />
            </TouchableOpacity>
          </View>
        )}
        {!(this.state.selected_yes || this.state.selected_no) && (
          <View style={styles.f_06} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  yesno_wrapper: {
    flex: 0.4,
    width: '80%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  btn_wrapper: {
    width: Btn_Width,
    height: Btn_Height,
    borderRadius: Math.round(Btn_Height / 2),
    justifyContent: 'center',
    alignItems: 'center',
  },
  picker_container: {
    flex: 0.6,
    width: '85%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  picker: {
    width: '30%',
    height: '70%',
  },
  picker_item: {
    color: 'white',
    fontSize: 26,
  },
  subcontainer_no: {
    flex: 0.6,
    width: '85%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  // nodetail_text: {
  //   fontFamily: Global.F_Avenir_Reg,
  //   fontWeight: 'normal',
  //   fontSize: 14,
  //   color: 'white',
  // },
  // nodetail_linktext: {
  //   fontFamily: Global.F_Avenir_Reg,
  //   fontWeight: 'normal',
  //   fontSize: 14,
  //   color: 'white',
  //   textDecorationLine: 'underline',
  //   textDecorationColor: 'white',
  // },
  f_06: {
    flex: 0.6,
  },
  spacer1: {
    marginTop: 15,
  },
});
