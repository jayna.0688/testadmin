import React from 'react';
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
// import Global from '@utils/GlobalValue';

export default class Answer4Screen extends React.Component {
  constructor() {
    super();
    this.state = {
      selection: 0,
    };
  }

  pressImage(index) {
    this.setState({
      selection: index,
    });
    const data = {
      screen: 4,
      save: index,
    };
    this.props.onChanged(data);
  }

  render() {
    const selection = this.state.selection;
    return (
      <View style={styles.container}>
        <View style={styles.spacer} />

        <View style={styles.topcontainer}>
          <View style={styles.topitem}>
            <TouchableOpacity
              onPress={this.pressImage.bind(this, 1)}
              style={{
                width: '100%',
                height: selection === 1 ? '90%' : '70%',
                marginBottom: 5,
              }}>
              <Image
                source={require('@images/Q4Sch.png')}
                style={styles.wh100}
                resizeMode="contain"
              />
            </TouchableOpacity>

            <TextS2 family="reg" size="12" color="white" label="Schubförmige" />
            <TextS2 family="reg" size="12" color="white" label="(remetierende)" />
          </View>

          <View style={styles.topitem}>
            <TouchableOpacity
              onPress={this.pressImage.bind(this, 2)}
              style={{
                width: '100%',
                height: selection === 2 ? '90%' : '70%',
                marginBottom: 5,
              }}>
              <Image
                source={require('@images/Q4Sek.png')}
                style={styles.wh100}
                resizeMode="contain"
              />
            </TouchableOpacity>

            <TextS2 family="reg" size="12" color="white" label="Sekundär" />
            <TextS2 family="reg" size="12" color="white" label="progrediente" />
          </View>
        </View>

        <View style={styles.spacer} />

        <View style={styles.bottomcontainer}>
          <TouchableOpacity
            onPress={this.pressImage.bind(this, 3)}
            style={{
              width: '100%',
              marginBottom: 5,
              height: selection === 3 ? '90%' : '70%',
            }}>
            <Image
              source={
                selection === 3
                  ? require('@images/Q4PriSelect.png')
                  : require('@images/Q4Pri.png')
              }
              style={styles.wh100}
              resizeMode="contain"
            />
          </TouchableOpacity>

          <TextS2 family="reg" size="12" color="white" label="Primär" />
          <TextS2 family="reg" size="12" color="white" label="progrediente" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  topcontainer: {
    width: '80%',
    height: '40%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  bottomcontainer: {
    width: '30%',
    height: '40%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  spacer: {
    height: '5%',
  },
  wh100: {
    width: '100%',
    height: '100%',
  },
  topitem: {
    width: '40%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  // item_wrapper: {
  //   width: '100%',
  //   height: '70%',
  //   marginBottom: 5,
  // },
  // item_text: {
  //   fontFamily: Global.F_Avenir_Reg,
  //   fontWeight: 'normal',
  //   fontSize: 14,
  //   color: 'white',
  // },
});
