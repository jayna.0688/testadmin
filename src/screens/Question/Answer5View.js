import React from 'react';
import {View, StyleSheet} from 'react-native';
// import Global from '@utils/GlobalValue';
// import isEmpty from '@utils/isEmpty';
// import {getData} from '@utils/GlobalFunction';
import Picker from 'react-native-wheel-picker';
var PickerItem = Picker.Item;
const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sept',
  'Oct',
  'Nov',
  'Dec',
];

export default class Answer5Screen extends React.Component {
  constructor() {
    super();
    this.state = {
      selected_year: 0,
      selected_month: 0, // 0~11
      years_list: [''],
      init_data: {
        year: 0,
        month: 0,
      },
    };
  }

  UNSAFE_componentWillMount = () => {
    const today = new Date();
    const today_year = today.getFullYear();
    const today_month = today.getMonth();

    let years_list = [];
    for (let i = -10; i <= 0; i++) {
      years_list.push((today_year + i).toString());
    }

    this.setState({
      years_list: years_list,
      selected_month: today_month,
      selected_year: years_list.length - 1,
      init_data: {
        year: years_list.length - 1,
        month: today_month,
      },
    });
  };

  onPickerSelect(picker, index) {
    let selected_month = this.state.selected_month;
    let selected_year = this.state.selected_year;
    if (picker === 1) {
      // month
      this.setState({
        selected_month: index,
      });
      selected_month = index;
    } else if (picker === 2) {
      // year
      this.setState({
        selected_year: index,
      });
      selected_year = index;
    }

    if (
      this.state.init_data.year !== selected_year ||
      this.state.init_data.month !== selected_month
    ) {
      let month_2num = (selected_month + 1).toString();
      if (selected_month < 9) {
        month_2num = '0' + month_2num;
      }
      const data_str =
        this.state.years_list[selected_year].toString() + month_2num;
      const data = {
        screen: 5,
        save: data_str,
      };
      this.props.onChanged(data);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.subcontainer}>
          <Picker
            style={styles.picker}
            selectedValue={this.state.selected_month}
            itemStyle={styles.picker_item}
            onValueChange={index => this.onPickerSelect(1, index)}>
            {months.map((value, i) => (
              <PickerItem label={value} value={i} key={'month' + value} />
            ))}
          </Picker>

          <Picker
            style={styles.picker}
            selectedValue={this.state.selected_year}
            itemStyle={styles.picker_item}
            onValueChange={index => this.onPickerSelect(2, index)}>
            {this.state.years_list.map((value, i) => (
              <PickerItem label={value} value={i} key={'money' + value} />
            ))}
          </Picker>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  subcontainer: {
    width: '80%',
    height: '70%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  picker: {
    width: '40%',
    height: '70%',
  },
  picker_item: {
    color: 'white',
    fontSize: 26,
  },
});
