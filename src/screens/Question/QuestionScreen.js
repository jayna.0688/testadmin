import React from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Animated,
  Easing,
} from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
import Global from '@utils/GlobalValue';
// import isEmpty from '@utils/isEmpty';
import {saveData} from '@utils/GlobalFunction';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import Answer1View from './Answer1View';
import Answer2View from './Answer2View';
import Answer3View from './Answer3View';
import Answer4View from './Answer4View';
import Answer5View from './Answer5View';
import Answer6View from './Answer6View';
import SelectionView from './SelectionView';
import {RoundButton} from '@component/Button';
const QSelectItemWidth = Math.round(Global.ScreenWidth * 0.25);
const QTitle1 = [
  'Ist bei dir eine Behandlung',
  'Was ist Dein',
  'Wie alt bist Du?',
  'Welche Form von MS',
  'Wann wurde Deine',
  'Welche Therapie(n)',
];
const QTitle2 = [
  'mit Ocrevus geplant?',
  'Geschlecht?',
  '',
  'hast Du?',
  'Diagnose gestellt?',
  'hast Du bislang erhalten?',
];
const QItemImages = [
  require('@images/Q1.png'),
  require('@images/Q2.png'),
  require('@images/Q3.png'),
  require('@images/Q4.png'),
  require('@images/Q5.png'),
  require('@images/Q6.png'),
];
const C_ICON_DESELECT = '#8C95BF';

export default class QuestionScreen extends React.Component {
  constructor(props) {
    super(props);
    const {params} = props.navigation.state;
    this.state = {
      isSelectionView: false,
      selectionViewDoValidation: false,
      availableNextScreen: false,

      screenIndex: 1, // 1~6
      savedScreens: (params && params.savedScreens) || '', // '1534'
      allSaved: false,
      showContinueMessage: false,
      loading: true,
    };
    this.onChanged = this.onChanged.bind(this);
    this.onPressItemSelectionView = this.onPressItemSelectionView.bind(this);
    this.animatedValue = new Animated.Value(0);
    this.pressFinish = this.pressFinish.bind(this);
  }

  UNSAFE_componentWillMount = () => {
    let screenIndex = 1;
    while (this.state.savedScreens.indexOf(`${screenIndex}`) !== -1) {
      screenIndex++;
    }
    if (screenIndex > 6) {
      this.setState({
        screenIndex: 6,
        allSaved: true,
        loading: false,
      });
    } else {
      if (screenIndex > 1) {
        this.setState({
          availableNextScreen: true,
        });
      }
      this.setState({
        screenIndex: screenIndex,
        loading: false,
      });
      if (this.state.savedScreens.length > 0) {
        this.startAnimationOfContinueMessage();
      }
    }
  };

  startAnimationOfContinueMessage() {
    this.setState({
      showContinueMessage: true,
    });
    this.animatedValue.setValue(0.01);
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 5000,
      easing: Easing.linear,
      delay: 500,
    }).start(() => {
      this.setState({
        showContinueMessage: false,
      });
    });
  }

  renderQSelectItems() {
    const screenIndex = this.state.screenIndex;
    let circleLocation = 2; // 1~3
    let secondScreenIndex = screenIndex;
    if (screenIndex === 1) {
      circleLocation = 1;
      secondScreenIndex = 2;
    } else if (screenIndex === 6) {
      circleLocation = 3;
      secondScreenIndex = 5;
    }

    let oneScreenSaved = [];
    const savedScreens = this.state.savedScreens;
    for (let i = 0; i < 3; i++) {
      if (savedScreens.indexOf(`${secondScreenIndex - 1 + i}`) !== -1) {
        oneScreenSaved[i] = true;
      } else {
        oneScreenSaved[i] = false;
      }
    }

    let groupQItems = [];
    for (let i = 1; i < 4; i++) {
      const borderWidth = circleLocation === i ? 4 : 0;
      groupQItems.push(
        <TouchableOpacity
          key={i}
          onPress={this.pressQScreenItem.bind(this, i)}
          style={[styles.qselection_one, {borderWidth: borderWidth}]}>
          {oneScreenSaved[i - 1] ? (
            <View style={styles.qselection_one_image}>
              <Image
                source={QItemImages[secondScreenIndex + i - 3]}
                style={styles.wh100}
                resizeMode="contain"
              />
            </View>
          ) : (
            <TextS2
              family="bold"
              size="30"
              color={Global.C_YELLOW}
              label={secondScreenIndex + i - 2}
            />
          )}
        </TouchableOpacity>,
      );
    }

    return <View style={styles.qselection}>{groupQItems}</View>;
  }
  pressQScreenItem(index) {
    // 1~3
    if (!this.state.availableNextScreen) {
      return;
    }
    let screenIndex = this.state.screenIndex;
    if (index === 1) {
      if (screenIndex === 6) {
        screenIndex = 4;
      } else if (screenIndex === 1) {
        return;
      } else {
        screenIndex--;
      }
    } else if (index === 2) {
      if (screenIndex === 6) {
        screenIndex = 5;
      } else if (screenIndex === 1) {
        screenIndex = 2;
      }
    } else {
      if (screenIndex === 6) {
        return;
      } else if (screenIndex === 1) {
        screenIndex = 3;
      } else {
        screenIndex++;
      }
    }
    this.setState({
      screenIndex: screenIndex,
    });
  }

  onChanged(data) {
    console.log('----onChanged', data);
    const screen = data.screen;
    if (screen === 1) {
      this.setState({
        availableNextScreen: true,
      });
    }
    let savedScreens = this.state.savedScreens;
    if (savedScreens.indexOf(`${screen}`) === -1) {
      savedScreens += screen;
      saveData('savedScreens', savedScreens);
      this.checkAllSaved(savedScreens);
      this.setState({
        savedScreens: savedScreens,
      });
    }
    const key = 'a' + screen.toString();
    const value = data.save.toString();
    saveData(key, value);
  }
  checkAllSaved(savedScreens) {
    if (savedScreens.length > 5) {
      this.setState({
        allSaved: true,
      });
    }
  }

  renderAnswerView() {
    const screenIndex = this.state.screenIndex;
    if (screenIndex === 1) {
      return <Answer1View onChanged={this.onChanged} />;
    } else if (screenIndex === 2) {
      return <Answer2View onChanged={this.onChanged} />;
    } else if (screenIndex === 3) {
      return <Answer3View onChanged={this.onChanged} />;
    } else if (screenIndex === 4) {
      return <Answer4View onChanged={this.onChanged} />;
    } else if (screenIndex === 5) {
      return <Answer5View onChanged={this.onChanged} />;
    } else if (screenIndex === 6) {
      return <Answer6View onChanged={this.onChanged} />;
    } else {
      return <View />;
    }
  }

  renderStatusIcon() {
    const isSelectionView = this.state.isSelectionView;
    const lefticon_width = isSelectionView ? 25 : 35;
    const righticon_oneline_width = isSelectionView ? 24 : 15;
    const righticon_oneline_height = isSelectionView ? 12 : 8;
    return (
      <TouchableOpacity
        disabled={!this.state.availableNextScreen}
        onPress={() =>
          this.setState({
            isSelectionView: !this.state.isSelectionView,
            selectionViewDoValidation: false,
          })
        }
        style={styles.topicon}>
        <View
          style={[
            styles.status_lefticon,
            {
              width: lefticon_width,
            },
          ]}>
          <OcticonsIcon
            name="primitive-dot"
            size={isSelectionView ? 7 : 10}
            color={isSelectionView ? C_ICON_DESELECT : 'white'}
          />
          <OcticonsIcon
            name="primitive-dot"
            size={isSelectionView ? 13 : 20}
            color={isSelectionView ? C_ICON_DESELECT : 'white'}
          />
          <OcticonsIcon
            name="primitive-dot"
            size={isSelectionView ? 7 : 10}
            color={isSelectionView ? C_ICON_DESELECT : 'white'}
          />
        </View>

        <View style={styles.status_righticon}>
          <View
            style={[
              styles.status_righticon_oneline,
              {
                width: righticon_oneline_width,
                height: righticon_oneline_height,
              },
            ]}>
            <OcticonsIcon
              name="primitive-dot"
              size={isSelectionView ? 15 : 7}
              color={isSelectionView ? 'white' : C_ICON_DESELECT}
            />
            <OcticonsIcon
              name="primitive-dot"
              size={isSelectionView ? 15 : 7}
              color={isSelectionView ? 'white' : C_ICON_DESELECT}
            />
          </View>
          <View
            style={[
              styles.status_righticon_oneline,
              {
                width: righticon_oneline_width,
                height: righticon_oneline_height,
              },
            ]}>
            <OcticonsIcon
              name="primitive-dot"
              size={isSelectionView ? 15 : 7}
              color={isSelectionView ? 'white' : C_ICON_DESELECT}
            />
            <OcticonsIcon
              name="primitive-dot"
              size={isSelectionView ? 15 : 7}
              color={isSelectionView ? 'white' : C_ICON_DESELECT}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  onPressItemSelectionView(screenIndex) {
    this.setState({
      isSelectionView: false,
      screenIndex: screenIndex,
      selectionViewDoValidation: false,
    });
  }

  pressFinish = () => {
    const savedScreens = this.state.savedScreens;
    if (savedScreens.length === 6) {
      this.props.navigation.navigate('FinishScreen');
    } else {
      this.setState({
        selectionViewDoValidation: true,
      });
    }
  };

  render() {
    if (this.state.loading) {
      return <View style={styles.container} />;
    }

    const screenIndex = this.state.screenIndex;
    let ContinueMessageLeft = -500;
    if (this.state.showContinueMessage) {
      ContinueMessageLeft = this.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [Global.ScreenWidth + 50, -300],
      });
    }
    const answerViewFlex =
      !this.state.allSaved && screenIndex === 6 ? 0.57 : 0.42;

    return (
      <View style={styles.container}>
        {/* scene type selector */}
        <View style={styles.topicon_wrapper}>{this.renderStatusIcon()}</View>

        {/* question selection items */}
        {!this.state.isSelectionView ? (
          <View style={styles.qselection_wrapper}>
            {this.renderQSelectItems()}
          </View>
        ) : (
          <View style={styles.f_076}>
            <SelectionView
              onPressItemSelectionView={this.onPressItemSelectionView}
              savedScreens={this.state.savedScreens}
              selectionViewDoValidation={this.state.selectionViewDoValidation}
            />
          </View>
        )}

        {!this.state.isSelectionView && <View style={styles.f_005} />}

        {/* title */}
        {!this.state.isSelectionView && (
          <View style={styles.title_wrapper}>
            <TextS2
              family="bold"
              size="20"
              color={'white'}
              label={QTitle1[screenIndex - 1]}
            />
            <TextS2
              family="bold"
              size="20"
              color={'white'}
              label={QTitle2[screenIndex - 1]}
            />
          </View>
        )}

        {/* answer content */}
        {!this.state.isSelectionView && (
          <View
            style={{
              flex: answerViewFlex,
            }}>
            {this.renderAnswerView()}
          </View>
        )}

        {/* finish button */}
        {(this.state.allSaved || this.state.isSelectionView) && (
          <View style={styles.finishbtn_wrapper}>
            <RoundButton label="FINISH" onPress={this.pressFinish} />
          </View>
        )}

        {this.state.showContinueMessage && (
          <Animated.View
            style={[styles.continuemessage, {left: ContinueMessageLeft}]}>
            <TextS2
              family="bold"
              size="20"
              color={'white'}
              label={'please continue setting'}
            />
          </Animated.View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: Global.C_BACKGROUND,
    flexDirection: 'column',
  },
  topicon_wrapper: {
    flex: 0.09,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 20,
  },
  qselection_wrapper: {
    flex: 0.2,
    alignItems: 'center',
  },
  f_076: {
    flex: 0.76,
  },
  f_005: {
    flex: 0.05,
  },
  title_wrapper: {
    zIndex: 10,
    flex: 0.09,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  finishbtn_wrapper: {
    flex: 0.15,
    width: '100%',
    paddingTop: 10,
    alignItems: 'center',
    backgroundColor: Global.C_BACKGROUND,
  },
  continuemessage: {
    position: 'absolute',
    bottom: 20,
  },
  topicon: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  qselection: {
    width: '90%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  qselection_one: {
    width: QSelectItemWidth,
    height: QSelectItemWidth,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Global.C_YELLOW,
    borderRadius: Math.round(QSelectItemWidth / 2),
  },
  qselection_one_image: {
    width: '50%',
    height: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  wh100: {
    width: '100%',
    height: '100%',
  },
  status_lefticon: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  status_righticon: {
    marginLeft: 15,
    flexDirection: 'column',
  },
  status_righticon_oneline: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
