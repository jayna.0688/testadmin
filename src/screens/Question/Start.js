import React from 'react';
import {View, StyleSheet} from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
import Global from '@utils/GlobalValue';
import {getData} from '@utils/GlobalFunction';
import {NavigationActions} from 'react-navigation';
import isEmpty from '@utils/isEmpty';
import {RoundButton} from '@component/Button';

export default class StartScreen extends React.Component {
  constructor(props) {
    // console.log('-------constructor');
    super(props);
    this.state = {
      loading: true,
    };
  }
  async UNSAFE_componentWillMount() {
    // console.log('-------componentWillMount');
    const savedScreens = await getData('savedScreens');
    if (!isEmpty(savedScreens)) {
      const navigateAction = NavigationActions.navigate({
        routeName: 'QuestionScreen',
        params: {
          savedScreens: savedScreens,
        },
      });
      this.props.navigation.dispatch(navigateAction);
      this.pressStart = this.pressStart.bind(this);
    } else {
      this.setState({
        loading: false,
      });
    }
  }
  pressStart = () => {
    this.props.navigation.navigate('QuestionScreen');
  };

  render() {
    if (this.state.loading) {
      return <View style={styles.container} />;
    }
    return (
      <View style={styles.container}>
        <TextS2 family="bold" size="34" color="white" label="Hallo" />

        <View style={styles.description_wrapper}>
          <TextS2
            family="reg"
            size="20"
            color="white"
            label="Please answer the"
          />
          <TextS2
            family="reg"
            size="20"
            color="white"
            label="following six questions"
          />
          <TextS2
            family="reg"
            size="20"
            color="white"
            label="that will help get you"
          />
          <TextS2
            family="reg"
            size="20"
            color="white"
            label="ready for your Ocrevus"
          />
          <TextS2 family="reg" size="20" color="white" label="treatment" />
        </View>

        <View style={styles.startbtn_wrapper}>
          <RoundButton label="START" onPress={this.pressStart} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  description_wrapper: {
    marginTop: 30,
    width: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Global.C_BACKGROUND,
  },
  startbtn_wrapper: {
    position: 'absolute',
    bottom: Math.round(Global.ScreenHeight * 0.1),
  },
});
