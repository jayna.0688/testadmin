import React from 'react';
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
import Global from '@utils/GlobalValue';
const QItemImages = [
  require('@images/Q1.png'),
  require('@images/Q2.png'),
  require('@images/Q3.png'),
  require('@images/Q4.png'),
  require('@images/Q5.png'),
  require('@images/Q6.png'),
];
const QItemImageWidth = Math.round(Global.ScreenWidth * 0.14);
const QItemWidth = Math.round(QItemImageWidth * 1.6);

export default class SelectionView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      savedScreens: props.savedScreens,
      // selectionViewDoValidation: props.selectionViewDoValidation || false,
    };
  }

  pressItem(screenIndex) {
    this.props.onPressItemSelectionView(screenIndex);
  }

  render() {
    const doValidation = this.props.selectionViewDoValidation || false;
    const savedScreens = this.state.savedScreens;
    const missingScreenCount = 6 - savedScreens.length;

    let groupImage = [];
    for (let i = 0; i < 3; i++) {
      let twogroup = [];
      for (let j = 0; j < 2; j++) {
        const screenIndex = i * 2 + j + 1;
        let ScreenSaved = false;
        if (savedScreens.indexOf(`${screenIndex}`) !== -1) {
          ScreenSaved = true;
        }
        const borderWidth = doValidation && !ScreenSaved ? 4 : 0;
        twogroup.push(
          <TouchableOpacity
            onPress={this.pressItem.bind(this, screenIndex)}
            key={`${i}${j}`}
            style={[styles.item_wrapper, {borderWidth: borderWidth}]}>
            {ScreenSaved ? (
              <Image
                source={QItemImages[i * 2 + j]}
                style={styles.item_image}
                resizeMode="contain"
              />
            ) : (
              <View>
                <TextS2
                  family="bold"
                  size="28"
                  color={Global.C_YELLOW}
                  label={screenIndex}
                />
              </View>
            )}
          </TouchableOpacity>,
        );
      }
      groupImage[i] = twogroup;
    }

    return (
      <View style={styles.container}>
        <View style={styles.f_015} />

        <View style={styles.items_wrapper}>
          <View style={styles.twoitems_wrapper}>{groupImage[0]}</View>

          <View style={styles.twoitems_wrapper}>{groupImage[1]}</View>

          <View style={styles.twoitems_wrapper}>{groupImage[2]}</View>
        </View>

        {doValidation ? (
          <View style={styles.missingwarning_container}>
            <View style={styles.missingwarning_wrapper}>
              <TextS2
                family="demi"
                size="16"
                color="white"
                label={`Please fill-in ${missingScreenCount} missing field`}
                textstyle={styles.missing_optionstyle}
              />
            </View>
            <View style={styles.triangle_wrapper}>
              <Image
                source={require('@images/triangle.png')}
                style={styles.wh100}
                resizeMode="contain"
              />
            </View>
          </View>
        ) : (
          <View style={styles.f_02} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item_wrapper: {
    width: QItemWidth,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Math.round(QItemWidth / 2),
    borderColor: Global.C_YELLOW,
  },
  container: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  f_015: {
    flex: 0.15,
  },
  items_wrapper: {
    flex: 0.65,
    width: '70%',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  twoitems_wrapper: {
    width: '100%',
    height: QItemWidth,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  // item_text: {
  //   fontFamily: Global.F_Avenir_Bold,
  //   fontWeight: 'bold',
  //   fontSize: 28,
  //   color: Global.C_YELLOW,
  // },
  item_image: {
    width: '70%',
    height: '70%',
  },
  triangle_wrapper: {
    width: 20,
    height: 10,
    top: -1,
  },
  wh100: {
    width: '100%',
    height: '100%',
  },
  f_02: {
    flex: 0.2,
  },
  missing_optionstyle: {
    lineHeight: 40,
  },
  missingwarning_wrapper: {
    width: '80%',
    height: 40,
    backgroundColor: Global.C_YELLOW,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  missingwarning_container: {
    flex: 0.2,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
