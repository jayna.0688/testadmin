import React from 'react';
import {View, Image, StatusBar, StyleSheet} from 'react-native';
import Global from '@utils/GlobalValue';

const DELAY_SECONDS = 2500; // 3000

export default class SplashScreen extends React.Component {
  UNSAFE_componentWillMount() {
    setTimeout(() => {
      this.props.navigation.navigate('App');
    }, DELAY_SECONDS);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />

        <Image
          source={require('@images/bk_splash.png')}
          style={styles.bk_image}
          resizeMode="contain"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Global.C_BACKGROUND,
  },
  bk_image: {
    width: '80%',
    height: 150,
  },
});
