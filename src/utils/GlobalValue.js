import { StyleSheet, Dimensions } from 'react-native';

const isDev = true; // true:develop, false:production

const { width, height } = Dimensions.get('window');
const ScreenHeight = width < height ? height : width; // height on portrait
const ScreenWidth = width < height ? width : height; // width on portrait

const AppColors = {
  C_BACKGROUND: '#5F6EB3',
  C_YELLOW: '#FFC72A',
  C_GRAYBACK: '#EDEDEE',
  C_YELLOWGREEN: '#B5BE00',
};

const AppFonts = {
  F_Avenir_Reg: 'AvenirNextLTPro-Regular',
  F_Avenir_Bold: 'AvenirNextLTPro-Bold',
  F_Avenir_Demi: 'AvenirNextLTPro-Demi',
};

const GlobalValue = {
  // // textinput maxlength
  // TM10: 10,
  // TM20: 20,
  // TM30: 30,
  // TM50: 50,
  // TM100: 100,
  // TM150: 150,
  // TM200: 200,
  // TM500: 500,
};

const GlobalStyles = StyleSheet.create({});

export default {
  isDev,
  ScreenWidth,
  ScreenHeight,
  ...AppColors,
  ...AppFonts,
  ...GlobalValue,
  ...GlobalStyles,
};
